package uk.co.adilparvez.markovchainsimulator;

public enum ActionMode {
	ADD_STATE,
	DELETE_STATE,
	SELECT_PAIR_OF_STATES,
	NO_EDIT,
	SELECT_STATE
}
