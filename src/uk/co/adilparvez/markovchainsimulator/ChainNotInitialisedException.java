package uk.co.adilparvez.markovchainsimulator;

@SuppressWarnings("serial")
public class ChainNotInitialisedException extends Exception {

	public ChainNotInitialisedException(String msg) {
		super(msg);
	}
	
}
