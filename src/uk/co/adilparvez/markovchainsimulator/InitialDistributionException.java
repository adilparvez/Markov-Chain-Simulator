package uk.co.adilparvez.markovchainsimulator;

@SuppressWarnings("serial")
public class InitialDistributionException extends Exception {
	
	public InitialDistributionException(String msg) {
		super(msg);
	}

}
