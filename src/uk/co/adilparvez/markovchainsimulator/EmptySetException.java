package uk.co.adilparvez.markovchainsimulator;

@SuppressWarnings("serial")
public class EmptySetException extends Exception {
	
	public EmptySetException(String msg) {
		super(msg);
	}

}
