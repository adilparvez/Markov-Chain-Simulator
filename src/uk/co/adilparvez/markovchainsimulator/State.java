package uk.co.adilparvez.markovchainsimulator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Random;

public class State {
	
	private LinkedHashMap<State, Double> neighbours = new LinkedHashMap<State, Double>();
	private double intitialProbability = 0;
	private boolean enabled = false;
	private boolean hover = true;
	private boolean selected = false;
	private final static int SIZE = 15;
	private int xCoor, yCoor;
	private boolean draggable = false;
	
	public State(int xCoor, int yCoor) {
		neighbours.put(this, 1.0);
		this.xCoor = xCoor;
		this.yCoor = yCoor;
	}
	
	public void deleteState(ArrayList<State> stateSpace) {
		for (State state : stateSpace) {
			state.deleteNeighbour(this);
		}
		stateSpace.remove(this);
	}
	
	public double getInitialProbability() {
		return intitialProbability;
	}
	
	public void setInitialProbability(double prob) {
		if (0 <= prob && prob <= 1) {
			intitialProbability = prob;
		}
	}
	
	public void addNeighbour(State next, double prob) {
		if (0 < prob && prob <= 1 && neighbours.get(this) >= 0) {
			neighbours.put(next, prob);
			neighbours.put(this, neighbours.get(this) - prob);
		}
	}
	
	public void deleteNeighbour(State neighbour) {
		neighbours.remove(neighbour);
	}
	
	public LinkedHashMap<State, Double> getNeighbours() {
		return neighbours;
	}
	
	public State getNextState() {
		double target = new Random().nextDouble();
		double probSum = 0;
		State next = null;
		for (State neighbour : neighbours.keySet()) {
			probSum += neighbours.get(neighbour);
			if (target < probSum) {
				next = neighbour;
				break;
			}
		}
		return next;
	}
	
	public boolean isMouseOver(MouseEvent e) {
		int xMouse = e.getX();
		int yMouse = e.getY();
		if (Math.pow(xMouse - xCoor, 2) + Math.pow(yMouse - yCoor, 2) <= Math.pow(SIZE/2, 2)) {
			return true;
		}
		return false;
	}
	
	public boolean hovering() {
		return hover;
	}
	
	public void hoverOn() {
		hover = true;
	}
	
	public void hoverOff() {
		hover = false;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void select() {
		selected = true;
	}
	
	public void deselect() {
		selected = false;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void enable() {
		enabled = true;
	}
	
	public void disable() {
		enabled = false;
	}
	
	public int getX() {
		return xCoor;
	}
	
	public int getY() {
		return yCoor;
	}
	
	public void setX(int xCoor) {
		this.xCoor = xCoor;
	}
	
	public void setY(int yCoor) {
		this.yCoor = yCoor;
	}
	
	public boolean hasForwardBackwardTransitionsWith(State target, ArrayList<State> stateSpace) {
		if (getNeighbours().containsKey(target) && target.getNeighbours().containsKey(this)) {
			return true;
		}
		return false;
	}
	
	public boolean isDraggable() {
		return draggable;
	}
	
	public void enableDrag() {
		draggable = true;
	}
	
	public void disableDrag() {
		draggable = false;
	}
	
	void render(Graphics2D g) {
		if (enabled) {
			g.setColor(Color.GREEN);
			g.fillOval((int)(xCoor - SIZE), (int)(yCoor - SIZE), 2 * SIZE, 2 * SIZE);
			g.setColor(Color.BLACK);
		} else if (hover) {
			g.setColor(Color.RED);
		} else if (selected) {
			g.setColor(Color.BLUE);
		} else {
			g.setColor(Color.BLACK);
		}
		g.fillOval((int)(xCoor - SIZE/2), (int)(yCoor - SIZE/2), SIZE, SIZE);
		g.setColor(Color.BLACK);
	}

}
