package uk.co.adilparvez.markovchainsimulator;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Random;

import javax.swing.SwingUtilities;

public class MarkovChain {
	
	private ArrayList<State> stateSpace = new ArrayList<State>();
	private State currentState = null;
	private static final int MIN_STATE_SEPARATION = 20;
	private static final double ARC_DEFLECTION = 0.1;
	private ActionMode mode = ActionMode.ADD_STATE;
	private State[] selectedPair = new State[2];
	private State selected = null;
	private int index = 0;
	private boolean running = false;
	private static double EPSILON = Math.pow(10, -4);
	
	public MarkovChain() {}
	
	public void addState(MouseEvent e) {
		State state = new State(e.getX(), e.getY());
		stateSpace.add(state);
	}
	
	public ArrayList<State> getStateSpace() {
		return stateSpace;
	}
	
	public int getSizeOfStateSpace() {
		return stateSpace.size();
	}
	
	public void pause() {
		running = false;
	}
	public void unpause() {
		running = true;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public State getInitialState() {
		double target = new Random().nextDouble();
		double probSum = 0;
		State start = null;
		for (State state : stateSpace) {
			probSum += state.getInitialProbability();
			if (target < probSum) {
				start = state;
				break;
			}
		}
		return start;
	}
	
	public boolean isValidInitialDistribution() {
		double probSum = 0;
		for (State state : stateSpace) {
			probSum += state.getInitialProbability();
		}
		if (probSum <= 1 - EPSILON || probSum >= 1.0 + EPSILON) {
			return false;
		}
		return true;
		
	}
	
	public boolean hasValidTransitions() {
		for (State state : stateSpace) {
			double outProbSum = 0;
			LinkedHashMap<State, Double> neighbours = state.getNeighbours();
			for (State neighbour : neighbours.keySet()) {
				outProbSum += neighbours.get(neighbour);
			}
			if (outProbSum <= 1 - EPSILON || outProbSum >= 1.0 + EPSILON) {
				return false;
			}
		}
		return true;
	}

	public void initialiseChain() throws InitialDistributionException, InvalidTransitionsException, EmptySetException {
		if (stateSpace.isEmpty()) {
			throw new EmptySetException("State space is empty.");
		}
		for (State state : stateSpace) {
			if (state.isEnabled()) {
				state.disable();
			}
		}
		if (!isValidInitialDistribution()) {
			throw new InitialDistributionException("Probabilities don't sum to unity.");
		} else if (!hasValidTransitions()) {
			throw new InvalidTransitionsException("Invalid state transitions.");
		} else {
			currentState = getInitialState();
			currentState.enable();
		}
	}
	
	public void deinitialiseChain() {
		currentState = null;
		for (State state : stateSpace) {					
			state.disable();
		}
	}
	
	public void evolveChain() throws ChainNotInitialisedException {
		if (currentState != null) {
			currentState.disable();
			currentState = currentState.getNextState();
			currentState.enable();
		} else {
			throw new ChainNotInitialisedException("Chain uninitialised.");
		}
	}
	
	public boolean nearAnotherState(MouseEvent e) {
		if (stateSpace.isEmpty()) {
			return false;
		}
		double minDist = Double.POSITIVE_INFINITY;
		int xMouse = e.getX();
		int yMouse = e.getY();
		for (State state : stateSpace) {
			double dist = Math.sqrt(Math.pow(xMouse - state.getX(), 2) + Math.pow(yMouse - state.getY(), 2));
			if (dist < minDist) {
				minDist = dist;
			}
		}
		if (minDist < MIN_STATE_SEPARATION) {
			return true;
		}
		return false;
	}
	
	public void clearSelectedPair() {
		for (State state : selectedPair) {
			if (state != null) {
				state.deselect();
			}
		}
		index = 0;
	}
	
	public void clearSelected() {
		if (selected != null) {
			selected.deselect();
		}
		selected = null;
	}
	
	public void setInitialProbability(double prob) {
		if (selected != null) {
			selected.setInitialProbability(prob);
		}
	}
	
	public void addTransition(double prob) {
		if (selectedPair[0] != null && selectedPair[1] != null) {
			selectedPair[0].addNeighbour(selectedPair[1], prob);
		}
	}
	
	public void deleteTransition() {
		State start = selectedPair[0];
		State end = selectedPair[1];
		if (start != null && end != null) {
			start.deleteNeighbour(end);
		}
		clearSelectedPair();
	}
	
	public boolean isEditable() {
		return mode != ActionMode.NO_EDIT;
	}
	
	public void mouseClickedInteraction(MouseEvent e) {
		switch (mode) {
			case ADD_STATE:
				if (!nearAnotherState(e)) {
					addState(e);
				}
				break;
			case DELETE_STATE:
				ArrayList<State> copy = new ArrayList<State>();
				for (State state : stateSpace) {
					copy.add(state);
				}
				for (State state : copy) {
					if (state.hovering()) {
						state.deleteState(stateSpace);
					}
				}
				break;
			case SELECT_PAIR_OF_STATES:
				for (State state : stateSpace) {
					if (state.hovering() && index <= 1) {
						state.select();
						selectedPair[index] = state;
						index++;
					}
				}
				break;
			case SELECT_STATE:
				for (State state : stateSpace) {
					if (state.hovering() && selected == null) {
						state.select();
						selected = state;
					}
				}
			case NO_EDIT:
				break;
			default:
				break;
		}
	}
	
	public void mouseMovedInteraction(MouseEvent e) {
		for (State state : stateSpace) {
			if (state.isMouseOver(e) && !state.isSelected()) {
				state.hoverOn();
			} else {
				state.hoverOff();
			}
		}
	}
	
	public void mousePressedInteraction(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			for (State state : stateSpace) {
				if (state.isMouseOver(e)) {
					state.enableDrag();
				}
			}
		}
	}
	
	public void mouseReleasedInteraction(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			for (State state : stateSpace) {
				if (state.isDraggable()) {
					state.disableDrag();
				}
			}
		}
	}
	public void mouseDraggedInteraction(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			for (State state : stateSpace) {
				if (state.isDraggable()) {
					state.setX(e.getX());
					state.setY(e.getY());
				}
			}
		}
	}
	
	public void setActionMode(ActionMode mode) {
		this.mode = mode;
	}
	
	public double[] getPointOnArc(double xA, double yA, double xC, double yC) {
		double xBar = (xA + xC)/2;
		double yBar = (yA + yC)/2;
		double deltaX = xC - xA;
		double deltaY = yC - yA;
		double xCoord = xBar - ARC_DEFLECTION * deltaY;
		double yCoord = yBar + ARC_DEFLECTION * deltaX;
		return new double[]{xCoord, yCoord};
	}
	
	public double[] getCoordinatesOfCentre() {
		double xBar = 0;
		double yBar = 0;
		for (State state : stateSpace) {
			xBar += state.getX();
			yBar += state.getY();
		}
		xBar /= getSizeOfStateSpace();
		yBar /= getSizeOfStateSpace();
		return new double[]{xBar, yBar};
	}
	
	public void drawArc(Graphics2D g, State start, State end) {
		if ((start == end) && start.getNeighbours().get(end) > 0) {
			double[] centreCoords = getCoordinatesOfCentre();
			double theta = - Math.PI/4 + Math.atan2(start.getY() - centreCoords[1], start.getX() - centreCoords[0]);
			g.rotate(theta, start.getX(), start.getY());
			g.drawOval(start.getX() - 5, start.getY() - 5, 40, 40);
			g.rotate(-theta, start.getX(), start.getY());
		} else if (start.hasForwardBackwardTransitionsWith(end, stateSpace)) {
			double[] pointOnArc = getPointOnArc(start.getX(), start.getY(), end.getX(), end.getY());
			Path2D path = new Path2D.Double();
			path.moveTo(start.getX(), start.getY());
			path.curveTo(start.getX(), start.getY(), pointOnArc[0], pointOnArc[1], end.getX(), end.getY());
			g.draw(path);
		} else {
			g.drawLine(start.getX(), start.getY(), end.getX(), end.getY());
		}
	}
	
	public void render(Graphics2D g) {
		for (State state : stateSpace) {
			state.render(g);
			for (State neighbour : state.getNeighbours().keySet()) {
				drawArc(g, state, neighbour);
			}
			
		}
	}
	
}
