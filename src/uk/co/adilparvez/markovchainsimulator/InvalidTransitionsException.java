package uk.co.adilparvez.markovchainsimulator;

@SuppressWarnings("serial")
public class InvalidTransitionsException extends Exception {
	
	public InvalidTransitionsException(String msg) {
		super(msg);
	}

}
