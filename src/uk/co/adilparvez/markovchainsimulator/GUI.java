package uk.co.adilparvez.markovchainsimulator;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

@SuppressWarnings("serial")
public class GUI extends JFrame {
	
	private static final String TITLE = "Finite State Space Discrete Time Homogeneous Markov Chain Simulator by Adil Parvez";
	private static final int WIDTH = 1306;
	private static final int HEIGHT = 725;
	private static final double WIDTH_FACTOR = 0.8;
	private static final double HEIGHT_FACTOR = 0.8;
	private static final int MAX_STATES_PER_RING = 15;
	private static final double RING_FACTOR = 2D/3;
	
	public void initialise() {
		setTitle(TITLE);
		setResizable(false);
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		try { 
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception ex) {
		    ex.printStackTrace();
		}
		
		JPanel content = new JPanel();
		content.setSize(WIDTH, HEIGHT);
		content.setLayout(null);
		
		final MarkovChain markovChain = new MarkovChain();
		
		//============================================================
		
		final DrawPanel drawPanel = new DrawPanel(markovChain);
		drawPanel.setLocation(0, 0);
		content.add(drawPanel);
		
		//============================================================
		
		final JLabel modeLabel = new JLabel("Mode: Add State");
		modeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		Font modeFont = modeLabel.getFont();
		modeLabel.setFont(new Font(modeFont.getName(), Font.BOLD, modeFont.getSize()));
		modeLabel.setLocation(1100, 0);
		modeLabel.setSize(200, 25);
		content.add(modeLabel);
		
		//============================================================
		
		JButton addStateModeButton = new JButton("Add State");
		addStateModeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (markovChain.isEditable()) {
					markovChain.setActionMode(ActionMode.ADD_STATE);
					modeLabel.setText("Mode: Add State");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
			
		});
		addStateModeButton.setLocation(1100, 25);
		addStateModeButton.setSize(200, 30);
		content.add(addStateModeButton);
		
		//============================================================
		
		JButton deleteStateModeButton = new JButton("Delete State");
		deleteStateModeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (markovChain.isEditable()) {
					markovChain.setActionMode(ActionMode.DELETE_STATE);
					modeLabel.setText("Mode: Delete State");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
		
		});
		deleteStateModeButton.setLocation(1100, 55);
		deleteStateModeButton.setSize(200, 30);
		content.add(deleteStateModeButton);
		
		//============================================================
	
		final JLabel transitionLabel = new JLabel("Transitions");
		transitionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		Font transitionFont = transitionLabel.getFont();
		transitionLabel.setFont(new Font(transitionFont.getName(), Font.BOLD, transitionFont.getSize()));
		transitionLabel.setLocation(1100, 85);
		transitionLabel.setSize(200, 25);
		content.add(transitionLabel);
			
		//============================================================
		
		JButton selectPairOfStatesButton = new JButton("Select Pair of States");
		selectPairOfStatesButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (markovChain.isEditable()) {
					markovChain.setActionMode(ActionMode.SELECT_PAIR_OF_STATES);
					modeLabel.setText("Mode: Select Pair of States");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
			
		});
		selectPairOfStatesButton.setLocation(1100, 110);
		selectPairOfStatesButton.setSize(200, 30);
		content.add(selectPairOfStatesButton);
		
		//============================================================
		
		final JLabel probLabel1 = new JLabel("Enter transition probability");
		probLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		probLabel1.setLocation(1100, 140);
		probLabel1.setSize(200, 20);
		content.add(probLabel1);
			
		//============================================================
		
		final JTextField transitionProbTextField = new JTextField();
		transitionProbTextField.setLocation(1100, 160);
		transitionProbTextField.setSize(200, 25);
		content.add(transitionProbTextField);
			
		//============================================================
		
		JButton addTransitionButton = new JButton("Add Transition");
		addTransitionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String str = transitionProbTextField.getText();
				if (!str.isEmpty() && markovChain.isEditable()) {
					double prob = parse(str);
					markovChain.addTransition(prob);
					markovChain.setActionMode(ActionMode.SELECT_PAIR_OF_STATES);
					modeLabel.setText("Mode: Select Pair of States");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
			
		});
		addTransitionButton.setLocation(1100, 185);
		addTransitionButton.setSize(200, 30);
		content.add(addTransitionButton);
		
		//============================================================
		
		JButton deleteTransitionButton = new JButton("Delete Transition");
		deleteTransitionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (markovChain.isEditable()) {
					markovChain.setActionMode(ActionMode.SELECT_PAIR_OF_STATES);
					modeLabel.setText("Mode: Select Pair of States");
					markovChain.deleteTransition();
					repaint();
				}
			}
			
		});
		deleteTransitionButton.setLocation(1100, 215);
		deleteTransitionButton.setSize(200, 30);
		content.add(deleteTransitionButton);
		
		//============================================================
		
		final JLabel initialLabel = new JLabel("Initial distribution");
		initialLabel.setHorizontalAlignment(SwingConstants.CENTER);
		Font initialFont = transitionLabel.getFont();
		initialLabel.setFont(new Font(initialFont.getName(), Font.BOLD, initialFont.getSize()));
		initialLabel.setLocation(1100, 245);
		initialLabel.setSize(200, 25);
		content.add(initialLabel);
		
		//============================================================
		
		JButton selectStateButton = new JButton("Select State");
		selectStateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (markovChain.isEditable()) {
					markovChain.setActionMode(ActionMode.SELECT_STATE);
					modeLabel.setText("Mode: Select State");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
			
		});
		selectStateButton.setLocation(1100, 270);
		selectStateButton.setSize(200, 30);
		content.add(selectStateButton);
			
		//============================================================
		
		final JLabel probLabel2 = new JLabel("Enter initial probability");
		probLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		probLabel2.setLocation(1100, 300);
		probLabel2.setSize(200, 20);
		content.add(probLabel2);
			
		//============================================================
		
		final JTextField initialProbTextField = new JTextField();
		initialProbTextField.setLocation(1100, 320);
		initialProbTextField.setSize(200, 25);
		content.add(initialProbTextField);
			
		//============================================================
		
		JButton assignButton = new JButton("Assign Probability");
		assignButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String str = initialProbTextField.getText();
				if (!str.isEmpty() && markovChain.isEditable()) {
				double prob = parse(str);
					markovChain.setInitialProbability(prob);
					markovChain.setActionMode(ActionMode.SELECT_STATE);
					modeLabel.setText("Mode: Select State");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
			
		});
		assignButton.setLocation(1100, 345);
		assignButton.setSize(200, 30);
		content.add(assignButton);
		
		//============================================================
		
		JButton assignProbabilitiesUniformlyButton = new JButton("Assign Probabilities Uniformly");
		assignProbabilitiesUniformlyButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (markovChain.getSizeOfStateSpace() == 0) {
					String message = "State space is empty.";
					JOptionPane.showMessageDialog(new JFrame(), message, "ERROR!", JOptionPane.ERROR_MESSAGE);
				} else if (markovChain.isEditable()) {
					int numberOfStates = markovChain.getSizeOfStateSpace();
					double prob = 1.0/numberOfStates;
					for (State state : markovChain.getStateSpace()) {
						state.setInitialProbability(prob);
					}
					markovChain.setActionMode(ActionMode.SELECT_STATE);
					modeLabel.setText("Mode: Select State");
					markovChain.clearSelectedPair();
					markovChain.clearSelected();
					repaint();
				}
			}
			
		});
		assignProbabilitiesUniformlyButton.setLocation(1100, 375);
		assignProbabilitiesUniformlyButton.setSize(200, 30);
		content.add(assignProbabilitiesUniformlyButton);
			
		//============================================================
		
		final JLabel simulationLabel = new JLabel("Simulation");
		simulationLabel.setHorizontalAlignment(SwingConstants.CENTER);
		Font simulationFont = transitionLabel.getFont();
		simulationLabel.setFont(new Font(simulationFont.getName(), Font.BOLD, simulationFont.getSize()));
		simulationLabel.setLocation(1100, 405);
		simulationLabel.setSize(200, 25);
		content.add(simulationLabel);
		
		//============================================================
		
		JButton initialiseChainButton = new JButton("Initialise Chain");
		initialiseChainButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					markovChain.initialiseChain();
					markovChain.setActionMode(ActionMode.NO_EDIT);
					modeLabel.setText("Mode: No Edit");
					repaint();
				} catch (InitialDistributionException ex1) {
					String message = "Invalid initial distribution.";
					JOptionPane.showMessageDialog(new JFrame(), message, "ERROR!", JOptionPane.ERROR_MESSAGE);
					System.out.println(ex1.getMessage());
				} catch (InvalidTransitionsException ex2) {
					String message = "Invalid transition probabilities.";
					JOptionPane.showMessageDialog(new JFrame(), message, "ERROR!", JOptionPane.ERROR_MESSAGE);
					System.out.println(ex2.getMessage());
				} catch (EmptySetException ex3) {
					String message = "State space is empty.";
					JOptionPane.showMessageDialog(new JFrame(), message, "ERROR!", JOptionPane.ERROR_MESSAGE);
					System.out.println(ex3.getMessage());
				}
			}
			
		});
		initialiseChainButton.setLocation(1100, 430);
		initialiseChainButton.setSize(200, 30);
		content.add(initialiseChainButton);
		
		//============================================================
		
		JButton nextStateButton = new JButton("Next State");
		nextStateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					markovChain.evolveChain();
					markovChain.setActionMode(ActionMode.NO_EDIT);
					modeLabel.setText("Mode: No Edit");
					repaint();
				} catch (ChainNotInitialisedException ex1) {
					String message = "Chain uninitialised.";
					JOptionPane.showMessageDialog(new JFrame(), message, "ERROR!", JOptionPane.ERROR_MESSAGE);
					System.out.println(ex1.getMessage());
				}
			}
			
		});
		nextStateButton.setLocation(1100, 460);
		nextStateButton.setSize(200, 30);
		content.add(nextStateButton);
		
		//============================================================
		
		JButton playButton = new JButton("Play");
		playButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				markovChain.unpause();
				Runnable r = new Runnable() {
					
					@Override
					public void run() {
						while (markovChain.isRunning()) {
							try {
								markovChain.evolveChain();
								if (!markovChain.isEditable()) {
									markovChain.setActionMode(ActionMode.NO_EDIT);
									modeLabel.setText("Mode: No Edit");
								}
								Thread.sleep(200);
							} catch (ChainNotInitialisedException ex1) {
								String message = "Chain uninitialised.";
								JOptionPane.showMessageDialog(new JFrame(), message, "ERROR!", JOptionPane.ERROR_MESSAGE);
								System.out.println(ex1.getMessage());
								try {
									Thread.currentThread().join();
								} catch (InterruptedException exx1) {
									exx1.printStackTrace();
								}
							} catch (InterruptedException ex2) {
								ex2.printStackTrace();
							};
							repaint();
						}
					}
					
				};
				new Thread(r).start();
			}
			
		});
		playButton.setLocation(1100, 490);
		playButton.setSize(200, 30);
		content.add(playButton);
		
		//============================================================
		
		JButton pauseButton = new JButton("Pause");
		pauseButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				markovChain.pause();
			}
			
		});
		pauseButton.setLocation(1100, 520);
		pauseButton.setSize(200, 30);
		content.add(pauseButton);
		
		//============================================================
		
		JButton editButton = new JButton("Edit");
		editButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				markovChain.pause();
				markovChain.setActionMode(ActionMode.ADD_STATE);
				modeLabel.setText("Mode: Add State");
				markovChain.deinitialiseChain();
				repaint();
			}
			
		});
		editButton.setLocation(1100, 550);
		editButton.setSize(200, 30);
		content.add(editButton);
		
		//============================================================
		
		JButton autoLayoutButton = new JButton("Auto Layout");
		autoLayoutButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				markovChain.setActionMode(ActionMode.ADD_STATE);
				modeLabel.setText("Mode: Add State");
				markovChain.deinitialiseChain();
				int numberOfStates = markovChain.getSizeOfStateSpace();
				boolean drawFromTop = false;
				int statesPlaced = 0;
				int numberOfRings = (int) Math.ceil((double) numberOfStates / MAX_STATES_PER_RING);
				int ringNumber = 1;
				double scaleFactor = 1;
				int numberOfStatesInLastRing = numberOfStates - (numberOfRings - 1) * MAX_STATES_PER_RING;
				for (State state : markovChain.getStateSpace()) {
					if (ringNumber < numberOfRings) {
						int[] coords = getAutoCoordinates(statesPlaced % MAX_STATES_PER_RING, MAX_STATES_PER_RING, scaleFactor, drawFromTop);
						state.setX(coords[0]);
						state.setY(coords[1]);
						statesPlaced++;
						if (statesPlaced % MAX_STATES_PER_RING == 0) {
							ringNumber++;
							scaleFactor *= RING_FACTOR;
							drawFromTop ^= true;
						}
					} else if (numberOfStatesInLastRing == 1) {
						state.setX(DrawPanel.WIDTH/2);
						state.setY(DrawPanel.HEIGHT/2);
					} else {
						int[] coords = getAutoCoordinates(statesPlaced % MAX_STATES_PER_RING, numberOfStatesInLastRing, scaleFactor, drawFromTop);
						state.setX(coords[0]);
						state.setY(coords[1]);
						statesPlaced++;
					}
				}
				repaint();
			}
			
		});
		autoLayoutButton.setLocation(1100, 580);
		autoLayoutButton.setSize(200, 30);
		content.add(autoLayoutButton);
		
		//============================================================
				
		
		getContentPane().add(content);
		setVisible(true);
	}
	
	public static double parse(String str) {
		// TODO input sanitisation
		double prob = -1;
		if (str.contains("/")) {
			String[] strArray = str.split("/");
			prob = Double.parseDouble(strArray[0])/Double.parseDouble(strArray[1]);
		} else {
			prob = Double.parseDouble(str);
		}
		return prob;
	}
	
	public static int[] getAutoCoordinates(int stateIndex, int numberOfStates, double scaleFactor, boolean drawFromTop) {
		int drawWidth = DrawPanel.WIDTH;
		int drawHeight = DrawPanel.HEIGHT;
		double theta = 2 * Math.PI * stateIndex / numberOfStates;
		if (drawFromTop) {
			theta += Math.PI;
		}
		double xCoor = (drawWidth / 2) * (1 + WIDTH_FACTOR * scaleFactor *  Math.sin(theta));
		double yCoor = (drawHeight / 2) * (1 - HEIGHT_FACTOR * scaleFactor * Math.cos(theta));
		return new int[]{(int)xCoor, (int)yCoor};
	}
	
	public static void main(String[] args) {
		new GUI().initialise();
	}

}
