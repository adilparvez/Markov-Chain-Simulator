package uk.co.adilparvez.markovchainsimulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DrawPanel extends JPanel implements MouseListener, MouseMotionListener {
	
	static final int WIDTH = 1100;
	static final int HEIGHT = 700;
	private MarkovChain markovChain;
	
	DrawPanel(MarkovChain markovChain) {
		this.markovChain = markovChain;
		setSize(WIDTH, HEIGHT);
		setVisible(true);
		addMouseListener(this);
		addMouseMotionListener(this);
		setFocusable(true);
		requestFocus();
		setBackground(Color.WHITE);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		markovChain.render((Graphics2D) g);
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				markovChain.mouseClickedInteraction(e);
				repaint();
			}
			
		};
		new Thread(r).start();
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(final MouseEvent e) {
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				markovChain.mousePressedInteraction(e);
				repaint();
			}
			
		};
		new Thread(r).start();
	}

	@Override
	public void mouseReleased(final MouseEvent e) {
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				markovChain.mouseReleasedInteraction(e);
				repaint();
			}
			
		};
		new Thread(r).start();
	}

	@Override
	public void mouseDragged(final MouseEvent e) {
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				markovChain.mouseDraggedInteraction(e);
				repaint();
			}
			
		};
		new Thread(r).start();
	}

	@Override
	public void mouseMoved(final MouseEvent e) {
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				markovChain.mouseMovedInteraction(e);
				repaint();
			}
			
		};
		new Thread(r).start();
	}

}
