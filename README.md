Markov Chain Simulator
==================
------------------

A finite state space, discrete time, homogeneous Markov chain simulator for illustrating the behaviour of this type of stochastic process.

![screenshot001](/screenshot001.png)

![](/markov.gif)

------------------
